; ######################################################
; ## PROGRAMM:   RDISK  mit     (c) Markus Hoffmann   ##
; ##             Voreinstellungen                     ##
; ## Dateiname: RDxxxxKl.PRG im aktuellen oder        ##
; ##            AUTO-Ordner                           ##
; ##            xxxx Gr��e   l Laufwerk (A-P)         ##
; ##            wenn l='X' wird nach freiem Eintrag   ##
; ##                       gesucht                    ##
; ##            wenn xxxx ung�ltig, wird nachgefragt  ##
; ## V.1.01  1990                                     ##
; ######################################################

; Resetfeste Ramdisk   Letzte Bearbeitung: 15.2.1995



DIR_GR=7           ; Directory-Groesse
DIR_ANZ=16*dir_gr  ; Anzahl Directory-Eintr�ge

mem_bios4       = (bios4     -start_sysrout)
mem_bios7       = (bios7     -start_sysrout)
mem_media       = (media     -start_sysrout)
mem_laufw       = (laufw     -start_sysrout)
mem_old_bios4   = (old_bios4 -start_sysrout)
mem_old_bios7   = (old_bios7 -start_sysrout)
mem_old_media   = (old_media -start_sysrout)
mem_data_start  = (data_start-start_sysrout)
mem_num_cl      = (num_cl    -start_sysrout)
mem_bpb         = (bpb       -start_sysrout)
mem_test        = (test      -start_sysrout)


super_ssp:
MAIN:     movea.l 4(a7),a0         ; Basepage  --> a0
          move.l  #$100,d6         ; 256
          add.l   $C(a0),d6        ; +TEXT
          add.l   $14(a0),d6       ; +DATA
          add.l   $1C(a0),d6       ; +BSS   --> d6
          move.l  d6,-(a7)
          move.l  a0,-(a7)
          clr.w   -(sp)
          move.w  #$4A,-(a7)       ; Mshrink
          trap    #1
          lea     12(sp),sp
          tst.l   d0
          BNE     KMEM_ERR

          clr.l   -(sp)         ; in den Supervisormode schalten
          move    #32,-(sp)
          trap    #1
          addq.l  #6,sp
          move.l  d0,super_ssp

          move.b  $ffff8001.s,d0      ; Speicherkonfiguration
          and.l   #15,d0
          lea     mem_conv_tab(pc),a0
          move.b  (a0,d0.w),d0
          swap    d0
          clr     d0
          sub.l   #(blockend-start_sysrout),d0
          bclr    #0,d0             ; Adresse gerade machen
          MOVE.L  d0,START_mem_rout ; EINTRAGEN
          move.l  d0,a6              ; a6 ab jetzt tabu !
          cmp.l  #'Test',mem_test(a6); Ramdisk schon installiert ?
          bne.s   new_inSTALL      ; System noch nicht installiert

REINSTALL:BSR     SUPER03         ; HIERHER, WENN RAMDISK SCHON DA
          add.b   #'A',d0
          move.b  d0,outstringdrive
          lea     outstring(pc),a0
          BSR     PRINT
          moveq.l #0,d0
          move    mem_num_cl(a6),d0
          BSR     D0_TO_DEZ
          BSR     PRINT
          lea     outstring2(pc),a0
          BSR     PRINT
          BRA     TERM
          ;ende

new_install:  lea     STRING_1(pc),a0
              BSR     PRINT

; Programmname suchen :

        pea      (MAIN-128)(pc)   ; BASEPAGE+128
        move     #26,-(sp)         ; SETDTA
        trap     #1
        addq.l   #6,sp

        move     #1,-(sp)
        pea      searchnam(pc)
        move     #78,-(sp)
        trap     #1                ; SF_FIRST
        addq.l   #8,sp
        tst      d0                ; Programm im
        bmi.s    errncon           ; aktuellen Verzeichnis nicht gefunden

; Hier wurde der Filename gefunden :

auswert:
        moveq    #0,d0
        move.b   (main-128+30+7)(pc),d0 ; Laufwerk 'RDxxxxKl.PRG'
        cmp      #'P',d0              ; Laufwerk X ?
        bgt.s    laufwerk_finden
        cmp      #'B',d0
        blt.s    laufwerk_finden
        sub.b    #'A',d0
        bmi.s    laufwerk_finden
   ; Laufwerk eintragen:
          lea     laufw(pc),a0
          move    d0,(a0)
          add.b   #'A',d0
          move.b  d0,(outstringdrive-laufw)(a0)
          move.b  d0,(drive_ch-laufw)(a0)
          move.b  d0,(diskname-laufw)(a0)
          bra.s   gogoon
laufwerk_finden: bsr    drive_s
gogoon:   bsr     aget_fre

          moveq.l #4,d2
          lea     (main-128+30+2)(pc),a0  ; Gr�sse 'RDxxxxKl.PRG'
          moveq.l #0,d7
          subq.b  #1,d2 
\hook     move.b  (a0)+,d0
          subi.b  #'0',d0
          bmi.s     \out
          cmpi.b  #9,d0 
          bgt.s     \out
          mulu    #10,d7
          add.l   d0,d7 
          DBRA    D2,\hook
\OUT:
          move    #$4e75,wait_ret    ; nicht ganz sauber ...
          bra     \aout

errncon:  move   #1,-(sp)
          pea    searchnam2(pc)
          move   #78,-(sp)
          trap   #1
          addq.l #8,sp
          tst    d0
          beq.s  auswert
  ; Filename nicht gefunden :

  ; dann     Groesse erfragen und freien Laufwerkeintrag finden

          bsr     drive_s
get_fre:  bsr    aget_fre

     
          moveq   #0,d7 
          moveq   #0,d0 
          pea     INPUT10_buf(pc)                ; Gr��e eingeben
          move.w  #10,-(a7)  ;Cconrs
          trap    #1
          addq.l  #6,a7 

          moveq.l #0,d2
          lea     in_string(pc),a0
          move.b  anz_char(pc),d2
          subq.b  #1,d2 
\aloop    move.b  (a0)+,d0
          subi.b  #'0',d0
          bmi.s     \aout
          cmpi.b  #9,d0 
          bgt.s     \aout
          mulu    #10,d7
          add.l   d0,d7 
          DBRA    D2,\aloop
\aout     cmpi.w  #10,d7               ; >10 K ?
          bcc.s   INSTALL              ; ja, -->
          lea     KLEIN_GR(pc),a0      ; nein, nichts installieren
          BSR     PRINT
          BSR     INP_RET
          BRA     TERM

 include "INSTALLB.Q"

DRIVE_s:  move.l  $4c2,d1   ; Laufwerkflaggen
          moveq.l #0,d0     ; erstes Bit
dtst:     btst    d0,d1     ; Testen
          beq.s   eer       ; Frei ? Ja--> weiter
          addq    #1,d0     ; n�chstes Bit
          cmp     #32,d0    ; schon alle ?
          beq.s   dend      ; Ja,-->ende
          bra.s   dtst      ; und n�chste Runde
eer       lea     laufw(pc),a0
          move    d0,(a0)
          add.b   #'A',d0
          move.b  d0,(outstringdrive-laufw)(a0)
          move.b  d0,(drive_ch-laufw)(a0)
          move.b  d0,(diskname-laufw)(a0)
dend:     rts

aGET_FRE   lea     SPEICHER_1(pc),a0
          BSR     PRINT

          move.l  #-1,-(a7)
          move.w  #$48,-(a7) ;Malloc  
          trap    #1
          addq.l  #6,a7 
          subi.l  #$20000,d0       ; Bildschirmspeicher + 32 K Reservegr��e abziehen
          BPL     \POSITIV          ; REICHT SPEICHER AUS ?
          moveq   #0,d0            ; nein...
\positiv  move.l  d0,d4

          moveq   #10,d2
          lsr.l   d2,d0

          BSR     D0_TO_DEZ
          BSR     PRINT
          lea     SPEICHER_2(pc),a0
          BSR     PRINT
          lea     INPUT_GR��E(pc),a0
          BSR     PRINT

          rts

INP_RET:  lea     RET_DR�CK(pc),a0
          BSR     PRINT
WAIT_RET  BSR     INP2
          cmpi.b  #$D,d0
          bne.s   WAIT_RET
          rts 

KMEM_ERR  lea     KATA_ERROR(pc),a0
          BSR     PRINT
          bsr.s   d0_to_dez
          BSR     PRINT
          lea     kata_error2(pc),a0
          bsr.s   print
          bsr.s   inp_ret
          bra.s   term



; ####################################
; ## Routine wandelt wert in D0 in  ##
; ## Stringkette Dezimal, variabler ##
; ## L�nge. R�ckgabe: a0=Adresse    ##
; ## Zeichenkette                   ##
; ####################################
d0_to_dez:lea     dez_buf(pc),a0
          clr.b   (a0)
nextdec   divu    #10,d0           ; in Dezimalstring wandeln
          swap    d0
          addi.b  #'0',d0
          move.b  d0,-(a0)
          clr.w   d0
          swap    d0
          tst     d0               ; a0= stringadresse
          bne.s   nextdec
          rts
          dc.b '123456789'
dez_buf:  dc.b 0
;---------------------------------------

super03:        MOVE.L  $472,mem_old_bios7(a6)  ; Ramdiskroutinen installieren
                lea     mem_bios7(a6),a1
                move.l  a1,$472
                MOVE.L  $476,mem_old_bios4(a6)
                lea     mem_bios4(a6),a1
                move.l  a1,$476
                MOVE.L  $47E,mem_old_media(a6)
                lea     mem_media(a6),a1
                move.l  a1,$47E
                move    mem_laufw(a6),d0        ; Laufwerknummer
                move.l  $4C2,d1                 ; im System installieren
                Bset    D0,D1
                move.l  d1,$4C2
                rts

;----------------------
term:  move.l super_ssp(pc),-(sp)    ; Wieder Usermodus
       move   #32,-(sp)
       trap   #1
       addq.l #6,sp
       clr    -(sp)              ; Programm verlassen
       trap   #1
;----------------------
print: movem.l d0-a6,-(sp)
       move.l a0,-(sp)
       move   #9,-(sp)
       trap   #1
       addq.l #6,sp
       movem.l (sp)+,d0-a6
       rts
;----------------------
inp2:  movem.l a6,-(sp)
       move   #7,-(sp)
       trap   #1
       addq.l #2,sp
       movem.l (sp)+,a6
       rts
; ###################################################
 include "r_rout.q"
; ###################################################
       data
mem_conv_tab:  dc.b $04    ; 0 :  128 +    0 =  128 K   *
input10_buf:
Max_char:      dc.b $0A    ; 1 :  128 +  512 =  640 K
anz_char:      dc.b $22    ; 2 :  128 + 2048 = 2176 K
In_string:     dc.b $42    ; 3 :  128 + 4096 = 4224 K
               dc.b $08    ; 4 :  512 +    0 =  512 K   *
               dc.b $10    ; 5 :  512 +  512 = 1024 K   *
               dc.b $28    ; 6 :  512 + 2048 = 2560 K
               dc.b $48    ; 7 :  512 + 4096 = 4608 K
               dc.b $20    ; 8 : 2048 +    0 = 2048 K   *
               dc.b $28    ; 9 : 2048 +  512 = 2560 K
               dc.b $40    ;10 : 2048 + 2048 = 4096 K   *
               dc.b $60    ;11 : 2048 + 4096 = 6114 K
               dc.b $40    ;12 : 4096 +    0 = 4096 K
               dc.b $48    ;13 : 4096 +  512 = 4608 K
               dc.b $60    ;14 : 4096 + 2048 = 6114 K
               dc.b $80    ;15 : 4096 + 4096 = 8192 K

STRING_1    dc.b 27,'l',27,'p R-Disk Version 1.06e  1988-1991,1995 (c) Markus Hoffmann ',27,'q',13,10
            dc.b 10,' RDxxxxKl.PRG       Resetresistente Ram-Disk.',27,'e',10,13,10,0
INPUT_GR��E dc.b 13,10,' Gr��e der Ramdisk : ',0
KLEIN_GR    dc.b 13,10,' Gr��e zu klein. Ram-Disk nicht installiert.',13,10,0
INSTALL_MEL dc.b 13,10,' Ram-Disk installiert als Floppy '
DRIVE_CH    dc.b 'C:\',13,10,' Anmelden im Desktop nicht vergessen!',13,10,0
SPEICHER_ERR dc.b 13,10,10,' Soviel Speicher steht aber nicht zur Verf�gung ! Versuchen Sie es noch einmal ...',0
SPEICHER_1  dc.b 13,10,10,' Es stehen ',0
SPEICHER_2  dc.b ' KByte f�r die Ram-Disk zur Verf�gung.',0
RET_DR�CK   dc.b 13,10,10,' Bitte ',27,'p Return ',27,'q dr�cken.',27,'f',13,10,0
KATA_ERROR  dc.b 13,10,10,' Interner GEMDOS-Fehler: ',0
kata_error2 dc.b ' Speicher konnte nicht angefordert werden !!',13,10,0
outstring:  dc.b 13,27,'l',27,'p'
outstringdrive: dc.b 'C:\ ',0
outstring2:     dc.b ' Kbytes R-Disk (c) Markus Hoffmann V.1.06e ',27,'q',10,13,0
                  align
diskname:   dc.b 'C:\R-DISK.M_H',0
searchnam2: dc.b '\AUTO\'
searchnam:  dc.b 'RD????K?.PRG',0

                  align

 end

 
